package touchgroup.org.moavortrag;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.MotionEvent;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    TextView textView;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        textView = (TextView)findViewById(R.id.txtOutput);
        textView.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                textView.setText("");
                String s = "";
                for(int index = 0; index < event.getPointerCount(); index++)
                {
                    s += "tx ->x: " + event.getX(index) + " y: " + event.getY(index) + "\n";
                }
                textView.setText(s);
                return true;
            }
        });
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        textView.setText("");
        String s = "";
        for(int index = 0; index < event.getPointerCount(); index++)
        {
            s += "app ->x: " + event.getX(index) + " y: " + event.getY(index) + "/n";
        }
        textView.setText(s);
        Toast.makeText(this, s, Toast.LENGTH_SHORT);
        return true;
    }
}
